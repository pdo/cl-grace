;;; -*- Mode: Lisp; Package: CL-User -*-
;;;
;;; Copyright (c) 2009 - 2023 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; ASDF system definition for the CL-GRACE system.
;;;
(in-package :cl-user)

(asdf:defsystem :cl-grace
  :author "Paul Onions"
  :version "0.16"
  :licence "MIT"
  :description "CL-GRACE: an interface to the Grace 2D plotting program."
  :depends-on (:uiop)
  :components ((:file "packages")
               (:file "utils")
               (:file "grace-interface")
               (:file "grace-plot"))
  :serial t)
