;;; -*- Mode: Lisp; Package: CL-Grace -*-
;;;
;;; Copyright (c) 2009 - 2019 Paul Onions
;;; See LICENCE file for licence information.
;;;
;;; High-level API for the CL-GRACE system -- simplified plotting.
;;;
;;; See examples.lisp file.
;;;
(in-package :cl-grace)

(defvar *plot* nil
  "The currently active plot object.")

(defvar *graph* 0
  "The default graph number if not specified with :GRAPH argument.")

(defvar *dataset* 0
  "The default dataset number if not specified with :DATASET argument.")

(defvar *autoscale-graph* t
  "Automatically scale graph after PLOT operations.")

(defvar *autotick-graph* t
  "Automatically tick graph after PLOT operations.")

;; Plot setup
;;
(defun ensure-active-plot (&rest args &key name new &allow-other-keys)
  "Ensure *PLOT* contains an active plot.

If NEW is non-NIL then create a new plot with the given arguments,
otherwise call FIND-PLOT with the provided NAME, but if no plot is
found then create a new one."
  (setf *plot*
        (if new
            (apply #'new-plot (remove-kw-args '(:new) args))
            (apply #'find-plot name :create t (remove-kw-args '(:name) args)))))

(defun ensure-clear-graph (&key (plot *plot*) (graph *graph*))
  (delete-graph plot graph)
  (create-graph plot graph))

;; Commands
;;
(defgeneric cmd (obj &key &allow-other-keys)
  (:documentation "Extensible function to send commands to Grace."))

(defmethod cmd ((obj string) &key (plot *plot*))
  (send-command plot obj))

;; Layout
;;
(defgeneric layout (obj &key &allow-other-keys)
  (:documentation "Graph layout function."))

(defmethod layout ((obj sequence) &key (plot *plot*) (hgap 0.2) (vgap 0.4) (offset 0.1))
  (let ((nrows (or (elt obj 0) 1))
        (ncols (or (elt obj 1) 1)))
    (arrange-graphs plot nrows ncols :hgap hgap :vgap vgap :offset offset)
    (dotimes (n nrows)
      (dotimes (m ncols)
        (set-graph-legend plot (+ (* n ncols) m)
                          :location (list (* (+ m 0.5) (/ 1.0 ncols))
                                          (* (- (- nrows n) 0.5) (/ 1.0 nrows))))))))

(defmethod layout ((obj integer) &key (plot *plot*) (hgap 0.2) (vgap 0.4) (offset 0.1))
  (arrange-graphs plot obj 1 :hgap hgap :vgap vgap :offset offset))

(defgeneric graph (obj &key &allow-other-keys)
  (:documentation "Graph parameterization function."))

(defmethod graph ((obj integer) &key (plot *plot*) title subtitle legend-location
                                  xlabel xscale xmin xmax xtick-format xtick-precision
                                  xtick-major xtick-minor
                                  ylabel yscale ymin ymax ytick-format ytick-precision
                                  ytick-major ytick-minor)
  (without-refresh
    (when title (set-graph-title plot obj :title title))
    (when subtitle (set-graph-subtitle plot obj :subtitle subtitle))
    (when xlabel (set-graph-axes plot obj :xlabel xlabel))
    (when xscale (set-graph-axes plot obj :xscale xscale))
    (when xmin (set-graph-axes plot obj :xmin xmin))
    (when xmax (set-graph-axes plot obj :xmax xmax))
    (when xtick-format (set-graph-axes plot obj :xtick-format xtick-format))
    (when xtick-precision (set-graph-axes plot obj :xtick-precision xtick-precision))
    (when xtick-major (set-graph-axes plot obj :xtick-major xtick-major))
    (when xtick-minor (set-graph-axes plot obj :xtick-minor xtick-minor))
    (when ylabel (set-graph-axes plot obj :ylabel ylabel))
    (when yscale (set-graph-axes plot obj :yscale yscale))
    (when ymin (set-graph-axes plot obj :ymin ymin))
    (when ymax (set-graph-axes plot obj :ymax ymax))
    (when ytick-format (set-graph-axes plot obj :ytick-format ytick-format))
    (when ytick-precision (set-graph-axes plot obj :ytick-precision ytick-precision))
    (when ytick-major (set-graph-axes plot obj :ytick-major ytick-major))
    (when ytick-minor (set-graph-axes plot obj :ytick-minor ytick-minor))
    (when legend-location (set-graph-legend plot obj :location legend-location))))

(defmethod graph :after ((obj t) &key (plot *plot*) (refresh *refresh-plot*))
  (when refresh (refresh-plot plot)))

;; Plotting
;;
(defun flatten-plot-sequence (seq &key xy x y real imag)
  "Prepare a sequence of data points ready for plotting, return a vector.

Each datapoint of the sequence SEQ should be a real or complex number,
or an object which can be flattened to a list of such with a call to
the generic function `FLATTEN-DATUM'.  Similarly, if any are given,
the sequences XY, X and/or Y should consist of objects yielding lists
of (complex) numbers when flattened, and be of the same length as SEQ.

The sequences of lists of (complex) numbers are then mapped together
into a single sequence of lists of (complex) numbers, such that each
list in the sequence is the concatenation of the corresponding XY, X,
Y and SEQ number lists, in that order.

Each list of (complex) numbers in the sequence is then processed to
extract just the real and/or imaginary parts according to whether the
REAL and/or IMAG boolean arguments are non-NIL.  If both are non-NIL
then each number in each list will expand into two numbers: the real
and imaginary parts."
  (let ((seqs (list seq)))
    (when y (push y seqs))
    (when x (push x seqs))
    (when xy (push xy seqs))
    (let ((new-seq (apply #'map 'vector #'list seqs)))
      (map 'vector
           (cond ((and real (not imag))
                  (lambda (datapoints)
                    (mapcar #'realpart (flatten-datum datapoints))))
                 ((and (not real) imag)
                  (lambda (datapoints)
                    (mapcar #'imagpart (flatten-datum datapoints))))
                 ((and real imag)
                  (lambda (datapoints)
                    (apply #'append (mapcar (lambda (z)
                                              (list (realpart z) (imagpart z)))
                                            (flatten-datum datapoints))))))
           new-seq))))

(defun ensure-xy-vector (vec)
  "Ensure VEC contains both x points and y points (at least)."
    (if (and (> (length vec) 0)
             (or (not (typep (aref vec 0) 'sequence))
                 (eql (length (aref vec 0)) 1)))
        (loop
           :for x :from 0
           :for y :across vec
           :collect (list x y))
        vec))

(defgeneric replot (obj &rest args &key &allow-other-keys)
  (:documentation "Extensible function to update a plot with new data/options."))

(defmethod replot :after ((obj t) &key (plot *plot*) (refresh *refresh-plot*))
  (when refresh (refresh-plot plot)))

(defmethod replot ((seq sequence) &key (real t) imag xy x y (plot *plot*)
                                    (graph *graph*) (dataset *dataset*)
                                    line-type line-style line-width line-color line-pattern
                                    symbol-type symbol-style symbol-width symbol-size
                                    symbol-color symbol-pattern symbol-fill-color
                                    symbol-fill-pattern legend)
  "Update a sequence plot with new data and/or options.

The actual data to be plotted is constructed using the function
`FLATTEN-PLOT-SEQUENCE' on the SEQ, XY, X, Y, REAL and IMAG arguments."
  (let ((data (flatten-plot-sequence seq :real real :imag imag :xy xy :x x :y y)))
    (send-data plot graph dataset (ensure-xy-vector data) :refresh nil)
    (when (or line-type line-style line-width line-color line-pattern)
      (set-dataset-line plot graph dataset :type line-type :style line-style
                        :width line-width :color line-color :pattern line-pattern
                        :refresh nil))
    (when (or symbol-type symbol-style symbol-width symbol-size
              symbol-color symbol-pattern symbol-fill-color symbol-fill-pattern)
      (set-dataset-symbol plot graph dataset :type symbol-type :style symbol-style
                          :width symbol-width :size symbol-size :color symbol-color
                          :pattern symbol-pattern :fill-color symbol-fill-color
                          :fill-pattern symbol-fill-pattern :refresh nil))
    (when legend
      (set-dataset-legend plot graph dataset :text legend :refresh nil))))

(defgeneric plot (obj &rest args &key &allow-other-keys)
  (:documentation "Extensible function to plot objects in Grace."))

(defmethod plot :after ((obj t) &key (plot *plot*) (graph *graph*) (autoscale *autoscale-graph*)
                                  (autotick *autotick-graph*) (refresh *refresh-plot*))
  (when autoscale (autoscale-graph plot graph :refresh nil))
  (when autotick  (autotick-graph plot graph :refresh nil))
  (when refresh   (refresh-plot plot)))

(defmethod plot ((obj t) &rest args &key plot graph &allow-other-keys)
  "Plot an object in Grace.

Create a Grace `XY' graph and plot the given object in it using the
appropriate `REPLOT' method, with auto-scaling and auto-ticking."
  (apply #'ensure-active-plot args)
  (create-graph (or plot *plot*) (or graph *graph*))
  (set-graph-type (or plot *plot*) (or graph *graph*) "XY")
  (apply #'replot obj args))
