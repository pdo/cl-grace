;;; -*- Mode: Lisp; Package: CL-User -*-
;;;
;;; Copyright (c) 2009 - 2015 Paul Onions
;;; See LICENCE file for licence information.
;;;
(in-package :cl-user)

;; Simple example derived from the grace_np C-code example in the Grace user guide.
;;
(defun run-ex1 ()
  (grace:ensure-active-plot)
  (grace:cmd "g0 on")
  (grace:cmd "world xmax 100")
  (grace:cmd "world ymax 10000")
  (grace:cmd "xaxis tick major 20")
  (grace:cmd "xaxis tick minor 10")
  (grace:cmd "yaxis tick major 2000")
  (grace:cmd "yaxis tick minor 1000")
  (grace:cmd "s0 on")
  (grace:cmd "s0 symbol 1")
  (grace:cmd "s0 symbol size 0.3")
  (grace:cmd "s0 symbol fill pattern 1")
  (grace:cmd "s1 on")
  (grace:cmd "s1 symbol 1")
  (grace:cmd "s1 symbol size 0.3")
  (grace:cmd "s1 symbol fill pattern 1")
  (loop :for i :from 1 :to 100
        :do (progn
              (grace:cmd (format nil "g0.s0 point ~d, ~d" i i))
              (grace:cmd (format nil "g0.s1 point ~d, ~d" i (* i i)))
              (when (= (mod i 10) 0)
                (grace:cmd "redraw")
                (sleep 1)))))

;; Plot sin waveform
;;
;; Generate a sequence of y values, x indices provided automatically
;; by PLOT.
;;
(defun run-ex2 ()
  (let ((sin-wave (loop :repeat 1000
                     :for x :from 0.0 :by 0.01
                     :collect (sin x))))
    (grace:ensure-active-plot)
    (grace:ensure-clear-graph)
    (grace:plot sin-wave)))

;; Plot sin and cos waveforms
;;
;; Provide both X and Y values to PLOT.
;;
(defun run-ex3 ()
  (let ((xs (loop :repeat 1000
               :for x :from 0.0 :by 0.01
               :collect x)))
    (grace:ensure-active-plot)
    (grace:ensure-clear-graph)
    (grace:plot (mapcar #'sin xs) :x xs :dataset 0 :line-color 2)
    (grace:plot (mapcar #'cos xs) :x xs :dataset 1 :line-color 3)))

;; Parametric plot in the complex plane
;;
(defun run-ex4 ()
  (let ((pts (loop :repeat 628
                :for theta :from 0.0 :by 0.01
                :collect (cis theta))))
    (grace:ensure-active-plot)
    (grace:ensure-clear-graph)
    (grace:plot pts :real t :imag t)))

;; Two graphs on the same page
;;
(defun run-ex5 ()
  (let ((xs (loop :repeat 628
               :for x :from 0.0 :by 0.01
               :collect x)))
    (grace:ensure-active-plot)
    (grace:layout '(2 1))
    (grace:graph 0 :title "Sine and Cosine" :subtitle "Plotting sin(x) & cos(x)")
    (grace:graph 1 :title "Magnitude Squared" :subtitle "Plotting sin^2(x) & cos^2(x)")
    (grace:plot (mapcar #'sin xs) :x xs :graph 0 :dataset 0 :line-color :red   :legend "sin(x)")
    (grace:plot (mapcar #'cos xs) :x xs :graph 0 :dataset 1 :line-color :green :legend "cos(x)")
    (grace:plot (mapcar (lambda (x) (expt (sin x) 2)) xs) :x xs :graph 1 :dataset 0
                :line-color :red :line-style :dot :legend "sin^2(x)")
    (grace:plot (mapcar (lambda (x) (expt (cos x) 2)) xs) :x xs :graph 1 :dataset 1
                :line-color :green :line-style :dash :legend "cos^2(x)")))
